package com.mazapps.template.ui.searchinfo.viewmodel

import com.mazapps.template.data.db.dao.DaoSearch
import com.mazapps.template.data.model.Search
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

/**
 * @author morad.azzouzi on 14/11/2020.
 */
@HiltAndroidTest
class SearchInfoViewModelDatabaseTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var dao: DaoSearch

    @Before
    fun init() {
        hiltRule.inject()
    }

    @Test
    @Throws(Exception::class)
    fun insertSearchAndFetchTest() {
        val search = Search(
            12,
            "title",
            122,
            1222,
            "toto",
            "tata",
            12
        )
        dao.insert(search)
        dao.fetchRelatedSearch().test().assertValue {
            it[0] == search
        }
    }
}