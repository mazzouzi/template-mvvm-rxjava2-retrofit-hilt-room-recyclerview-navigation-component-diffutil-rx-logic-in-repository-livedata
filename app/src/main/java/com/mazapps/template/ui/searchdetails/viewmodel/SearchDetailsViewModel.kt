package com.mazapps.template.ui.searchdetails.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.mazapps.template.data.model.Search

/**
 * @author morad.azzouzi on 13/12/2020.
 */
class SearchDetailsViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    val searchData: Search? = savedStateHandle.get<Search>(SEARCH_DATA_KEY)

    companion object {
        const val SEARCH_DATA_KEY = "searchData"
    }

}