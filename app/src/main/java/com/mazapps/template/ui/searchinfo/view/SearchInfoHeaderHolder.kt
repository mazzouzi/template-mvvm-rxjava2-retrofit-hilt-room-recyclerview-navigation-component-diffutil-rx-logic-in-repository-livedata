package com.mazapps.template.ui.searchinfo.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.mazapps.template.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_search_info_header.*

/**
 * @author morad.azzouzi on 22/11/2020.
 */
class SearchInfoHeaderHolder(
    override val containerView: View
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(value: String) {
        keyword.text = itemView.resources.getString(R.string.keyword, value)
    }
}