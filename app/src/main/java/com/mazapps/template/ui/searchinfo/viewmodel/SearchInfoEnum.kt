package com.mazapps.template.ui.searchinfo.viewmodel

/**
 * @author morad.azzouzi on 22/11/2020.
 */
enum class SearchInfoEnum {
    HEADER,
    SEARCH
}