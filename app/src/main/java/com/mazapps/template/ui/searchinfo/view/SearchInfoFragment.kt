package com.mazapps.template.ui.searchinfo.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import android.widget.ViewFlipper
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import com.mazapps.template.R
import com.mazapps.template.data.StatusEnum
import com.mazapps.template.data.model.Search
import com.mazapps.template.helper.RxBusHelper
import com.mazapps.template.ui.interfaces.LoadingFlipperInterface
import com.mazapps.template.ui.searchinfo.diffutil.SearchInfoDiffCallback
import com.mazapps.template.ui.searchinfo.viewmodel.SearchInfoEnum
import com.mazapps.template.ui.searchinfo.viewmodel.SearchInfoViewModel
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_search_info.*
import javax.inject.Inject


@ExperimentalStdlibApi
@AndroidEntryPoint
class SearchInfoFragment : Fragment(), LoadingFlipperInterface {

    @Inject lateinit var viewModel: SearchInfoViewModel
    @Inject lateinit var disposables: CompositeDisposable
    @Inject lateinit var adapter: SearchInfoAdapter
    @Inject lateinit var itemDecoration: DividerItemDecoration

    override val viewFlipper: ViewFlipper
        get() = searchViewFlipper

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_search_info, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerEvent()
        observeLiveData()
        initView()
    }

    private fun observerEvent() {
        disposables.add(
            RxBusHelper
                .events
                .subscribe {
                    when (it) {
                        is SearchInfoClickEvent -> onSearchInfoItemClick(it)
                    }
                }
        )
    }

    private fun onSearchInfoItemClick(event: SearchInfoClickEvent) {
        val itemData = viewModel.getItemDataAt(event.position) as Search
        val action = SearchInfoFragmentDirections.actionSearchInfoToSearchDetails(itemData)
        findNavController().navigate(action)
    }

    private fun observeLiveData() {
        viewModel.items.observe(viewLifecycleOwner) {
            when (it.status) {
                StatusEnum.LOADING -> {
                    showLoaderView()
                }
                StatusEnum.SUCCESS -> {
                    onFetchSearchInfoSuccess(it.data)
                }
                StatusEnum.ERROR -> {
                    onFetchSearchInfoError()
                }
            }
        }
    }

    private fun onFetchSearchInfoSuccess(data: MutableList<Pair<SearchInfoEnum, Any>>?) {
        data?.let {
            val copy = ArrayList(it)
            viewModel.pendingUpdates.add(copy)
            if (viewModel.pendingUpdates.size > 1) return
            updateItems(copy)
        }
    }

    private fun updateItems(items: MutableList<Pair<SearchInfoEnum, Any>>) {
        disposables.add(
            Observable
                .just(items)
                .map {
                    DiffUtil.calculateDiff(SearchInfoDiffCallback(viewModel.currentList, it), true)
                }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        viewModel.updateCurrentList(items)
                        adapter.dispatchUpdates(it)
                        showContent()

                        viewModel.pendingUpdates.removeFirstOrNull()
                        if (viewModel.pendingUpdates.size > 0) {
                            updateItems(viewModel.pendingUpdates.removeFirst())
                        }
                    },
                    {
                        Toast.makeText(context, getString(R.string.error_occured), LENGTH_SHORT).show()
                        showNoResultView()
                    }
                )
        )
    }

    private fun onFetchSearchInfoError() {
        showNoResultView()
        adapter.notifyDataSetChanged()
    }

    private fun initView() {
        recyclerView.setHasFixedSize(true)
        recyclerView.addItemDecoration(itemDecoration)
        recyclerView.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        if (!viewModel.hasData()) {
            disposables.add(viewModel.fetchSearchInfo())
        }  else {
            showContent()
            adapter.notifyDataSetChanged()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposables.clear()
    }
}