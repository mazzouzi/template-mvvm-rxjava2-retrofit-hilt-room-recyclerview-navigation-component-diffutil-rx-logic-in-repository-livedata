package com.mazapps.template.ui.searchinfo.view

data class SearchInfoClickEvent(val position: Int)