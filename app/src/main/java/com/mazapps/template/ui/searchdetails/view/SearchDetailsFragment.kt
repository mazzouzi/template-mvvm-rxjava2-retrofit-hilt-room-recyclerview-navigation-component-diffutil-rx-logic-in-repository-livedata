package com.mazapps.template.ui.searchdetails.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.mazapps.template.R
import com.mazapps.template.ui.searchdetails.viewmodel.SearchDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_search_details.*

@AndroidEntryPoint
class SearchDetailsFragment : Fragment() {

    private val viewModel: SearchDetailsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_search_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        title.text = viewModel.searchData?.title
        wordCount.text = viewModel.searchData?.wordcount.toString()
        pageId.text = viewModel.searchData?.pageid.toString()
        size.text = viewModel.searchData?.size.toString()
    }
}