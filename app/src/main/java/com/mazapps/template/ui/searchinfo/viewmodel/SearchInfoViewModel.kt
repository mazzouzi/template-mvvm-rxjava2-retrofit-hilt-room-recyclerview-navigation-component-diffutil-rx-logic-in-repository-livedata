package com.mazapps.template.ui.searchinfo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mazapps.template.data.DataManager
import com.mazapps.template.data.Resource
import com.mazapps.template.data.model.Search
import io.reactivex.disposables.Disposable

/**
 * @author morad.azzouzi on 11/11/2020.
 */
@ExperimentalStdlibApi
class SearchInfoViewModel(private val dataManager: DataManager) : ViewModel() {

    private val _items = MutableLiveData<Resource<MutableList<Pair<SearchInfoEnum, Any>>>>()
    val items: LiveData<Resource<MutableList<Pair<SearchInfoEnum, Any>>>>
        get() = _items
    private val data: MutableList<Pair<SearchInfoEnum, Any>>?
        get() = items.value?.data

    init {
        _items.value = Resource.loading()
    }

    val pendingUpdates = ArrayDeque<MutableList<Pair<SearchInfoEnum, Any>>>()
    var currentList: MutableList<Pair<SearchInfoEnum, Any>> = mutableListOf()

    val itemCount: Int
        get() = data?.size ?: 0
    fun getItemViewTypeAt(position: Int): Int = data?.get(position)?.first?.ordinal ?: 0
    fun getItemDataAt(position: Int): Any? = data?.get(position)?.second

    fun hasData(): Boolean = data?.any { it.first == SearchInfoEnum.SEARCH } ?: false

    fun updateCurrentList(list: MutableList<Pair<SearchInfoEnum, Any>>) {
        currentList.clear()
        currentList.addAll(list)
    }

    fun fetchSearchInfo(): Disposable =
        dataManager
            .fetchSearchInfo("query", "json", "search", KEYWORD)
            .subscribe(
                {
                    createRelatedSearchData(it)
                },
                {
                    handleFetchSearchInfoError()
                }
            )

    private fun createRelatedSearchData(search: List<Search>?) {
        if (!search.isNullOrEmpty()) {
            mutableListOf<Pair<SearchInfoEnum, Any>>().apply {
                add(Pair(SearchInfoEnum.HEADER, KEYWORD))
                addAll(search.map { Pair(SearchInfoEnum.SEARCH, it) })
            }.also {
                handleFetchSearchInfoSuccess(it)
            }
        } else {
            handleFetchSearchInfoError()
        }
    }

    private fun handleFetchSearchInfoSuccess(list: MutableList<Pair<SearchInfoEnum, Any>>) {
        _items.postValue(Resource.success(list))
    }

    private fun handleFetchSearchInfoError() {
        if (!hasData()) {
            _items.postValue(Resource.error())
        }
    }

    companion object {
        const val KEYWORD = "sasuke"
    }
}