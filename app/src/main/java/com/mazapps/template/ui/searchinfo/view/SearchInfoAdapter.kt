package com.mazapps.template.ui.searchinfo.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mazapps.template.R
import com.mazapps.template.data.model.Search
import com.mazapps.template.ui.searchinfo.viewmodel.SearchInfoEnum
import com.mazapps.template.ui.searchinfo.viewmodel.SearchInfoViewModel

@ExperimentalStdlibApi
class SearchInfoAdapter(
    private val viewModel: SearchInfoViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun dispatchUpdates(diffResult: DiffUtil.DiffResult) {
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            SearchInfoEnum.SEARCH.ordinal -> SearchInfoHolder(
                LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.item_search_info, parent, false)
            )
            else -> SearchInfoHeaderHolder(
                LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.item_search_info_header, parent, false)
            )
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is SearchInfoHolder -> holder.bind(viewModel.getItemDataAt(position) as Search)
            is SearchInfoHeaderHolder -> holder.bind(viewModel.getItemDataAt(position) as String)
        }
    }

    override fun getItemCount(): Int = viewModel.itemCount

    override fun getItemViewType(position: Int): Int = viewModel.getItemViewTypeAt(position)


}
