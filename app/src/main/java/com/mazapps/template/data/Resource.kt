package com.mazapps.template.data

/**
 * @author morad.azzouzi on 13/12/2020.
 */
data class Resource<out T>(val status: StatusEnum, val data: T?, val message: String?) {

    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(StatusEnum.SUCCESS, data, null)
        }

        fun <T> error(data: T? = null, msg: String? = null): Resource<T> {
            return Resource(StatusEnum.ERROR, data, msg)
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(StatusEnum.LOADING, data, null)
        }
    }
}

enum class StatusEnum {
    LOADING,
    SUCCESS,
    ERROR
}

