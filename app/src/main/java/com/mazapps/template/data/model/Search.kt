package com.mazapps.template.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "related_search")
@Parcelize
data class Search(
    val ns: Int,
    @PrimaryKey val title: String,
    val pageid: Int,
    val size: Int,
    val snippet: String,
    val timestamp: String,
    val wordcount: Int
) : Parcelable