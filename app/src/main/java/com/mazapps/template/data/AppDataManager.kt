package com.mazapps.template.data

import com.mazapps.template.data.db.DbHelper
import com.mazapps.template.data.model.Search
import com.mazapps.template.data.network.ApiHelper
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author morad.azzouzi on 01/12/2019.
 */
class AppDataManager @Inject constructor(
    private val apiHelper: ApiHelper,
    private val dbHelper: DbHelper
) : DataManager {

    override fun fetchSearchInfo(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Observable<List<Search>?> =
        Observable
            .concatArrayEagerDelayError(
                dbHelper.fetchRelatedSearch().toObservable(),
                apiHelper
                    .fetchSearchInfo(action, format, list, keyword)
                    .flatMap {
                        Observable.just(it.body()?.query?.search)
                    }
                    .doAfterNext {
                        it?.forEach { search -> dbHelper.insert(search) }
                    }
            )
            .subscribeOn(Schedulers.io())
}