package com.mazapps.template.di.module

import com.mazapps.template.data.DataManager
import com.mazapps.template.ui.searchinfo.viewmodel.SearchInfoViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped

/**
 * @author morad.azzouzi on 11/11/2020.
 */
@Module
@InstallIn(ActivityRetainedComponent::class)
class ActivityRetainedModule {

    @ExperimentalStdlibApi
    @ActivityRetainedScoped
    @Provides
    fun provideSearchInfoViewModel(
        dataManager: DataManager
    ): SearchInfoViewModel = SearchInfoViewModel(dataManager)
}