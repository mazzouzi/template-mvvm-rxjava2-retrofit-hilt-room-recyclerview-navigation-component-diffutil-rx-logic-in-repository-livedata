package com.mazapps.template.di.module

import com.mazapps.template.data.db.AppDbHelper
import com.mazapps.template.data.db.DbHelper
import com.mazapps.template.data.network.ApiHelper
import com.mazapps.template.data.network.AppApiHelper
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * @author morad.azzouzi on 12/11/2020.
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class ApiModule {

    /**
     * The @Binds annotation tells Hilt which implementation to use when it needs to provide
     * an instance of an interface.
     */
    @Binds
    @Singleton
    abstract fun bindApiHelper(appApiHelper: AppApiHelper): ApiHelper

    @Binds
    @Singleton
    abstract fun bindDbHelper(appDbHelper: AppDbHelper): DbHelper
}